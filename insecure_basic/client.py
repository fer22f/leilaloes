from socket import create_connection

from config import SERVER_ADDRESS
from message import WelcomeMessage
from postofficer import PostOfficer


class Client:
    def create_simple_socket(self):
        return create_connection(SERVER_ADDRESS)

    def create_socket(self):
        return self.create_simple_socket()

    def run(self):
        self.post_officer = PostOfficer(self.create_socket(), SERVER_ADDRESS)

        self.enter_auction()

        self.post_officer.close()

    def enter_auction(self):
        message = self.post_officer.receive_message()
        if isinstance(message, WelcomeMessage):
            print(f"Auctioned item: {message.item_name}")
        else:
            raise Exception("No welcome message received from server")


if __name__ == "__main__":
    Client().run()
