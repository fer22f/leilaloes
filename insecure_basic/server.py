from socket import create_server
from threading import Thread

from config import PORT
from message import WelcomeMessage
from postofficer import PostOfficer


class Server:
    def __init__(self):
        self.post_office = []
        self.listener_socket = None

    def run(self):
        self.item_name = input("What is the name of the item to be auctioned? ")
        Thread(target=self.wait_for_clients, args=()).start()

        print("Press enter to close all connections.")
        self.close_connections()

    def create_simple_socket(self):
        return create_server(("", PORT))

    def create_socket(self):
        return self.create_simple_socket()

    def wait_for_clients(self):
        self.listener_socket = self.create_socket()
        while True:
            try:
                socket, address = self.listener_socket.accept()
                Thread(
                    target=self.handle_client, args=(PostOfficer(socket, address),)
                ).start()
            except ConnectionAbortedError:
                # When the listener_socket is closed by the main thread
                break

    def handle_client(self, post_officer):
        self.post_office.append(post_officer)

        post_officer.send_message(WelcomeMessage(self.item_name))

    def close_connections(self):
        for post_officer in self.post_office:
            post_officer.close()

        self.listener_socket.close()


if __name__ == "__main__":
    Server().run()
