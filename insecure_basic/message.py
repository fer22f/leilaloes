class Message:
    def __init__(self):
        self.data = dict()

    def serialize(self):
        message = {"type": self.type}
        message["data"] = self.data
        return message


class WelcomeMessage(Message):
    type = "Welcome"

    def __init__(self, item_name):
        super().__init__()
        self.item_name = item_name
        self.data["item_name"] = item_name


MESSAGES = (WelcomeMessage,)
