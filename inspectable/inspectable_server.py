from secure_server import SecureServer

from inspectable import InspectableListenerSocket


class InspectableSecureServer(SecureServer):
    def create_socket(self):
        socket = self.create_simple_socket()
        return InspectableListenerSocket(socket, self.context, server_side=True)


if __name__ == "__main__":
    InspectableSecureServer().run()
