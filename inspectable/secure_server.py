from ssl import PROTOCOL_TLS_SERVER, SSLContext

from config import CERTIFICATE_FILE, PRIVATE_KEY_FILE
from server import Server


class SecureServer(Server):
    def __init__(self):
        super().__init__()
        context = SSLContext(PROTOCOL_TLS_SERVER)
        context.load_cert_chain(CERTIFICATE_FILE, PRIVATE_KEY_FILE)
        self.context = context

    def create_socket(self):
        socket = self.create_simple_socket()
        return self.context.wrap_socket(socket, server_side=True)


if __name__ == "__main__":
    SecureServer().run()
