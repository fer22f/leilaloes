import sys

from secure_client import SecureClient

from inspectable import InspectableSocket


class InspectableSecureClient(SecureClient):
    def __init__(self, allow_editing=False):
        super().__init__()
        self.allow_editing = allow_editing

    def create_socket(self):
        socket = self.create_simple_socket()
        socket = InspectableSocket(
            socket,
            self.context,
            allow_editing=self.allow_editing,
            server_hostname=self.hostname,
        )
        socket.do_handshake()
        return socket


if __name__ == "__main__":
    allow_editing = "--edit" in sys.argv
    InspectableSecureClient(allow_editing=allow_editing).run()
