from socket import create_server
from ssl import SSLEOFError
from threading import Thread

from config import PORT
from message import BidMessage, BillMessage, LosingMessage, PaymentMessage, WelcomeMessage
from postofficer import PostOfficer


class Server:
    def __init__(self):
        self.bids = []
        self.post_office = []
        self.listener_socket = None

    def run(self):
        self.item_name = input("What is the name of the item to be auctioned? ")
        Thread(target=self.wait_for_clients, args=()).start()

        print("Press enter to end the auction.")
        while True:
            input()
            if len(self.bids) > 0:
                break
            print("The auction needs at least one bid before it can be ended.")

        self.end_auction()

    def create_simple_socket(self):
        return create_server(("", PORT))

    def create_socket(self):
        return self.create_simple_socket()

    def wait_for_clients(self):
        self.listener_socket = self.create_socket()
        while True:
            try:
                socket, address = self.listener_socket.accept()
                Thread(
                    target=self.handle_client, args=(PostOfficer(socket, address),)
                ).start()
            except ConnectionAbortedError:
                # When the listener_socket is closed by the main thread
                break
            except SSLEOFError:
                # When a handshake is unexpectedly closed, we ignore
                print("Client didn't finish handshake")

    def handle_client(self, post_officer):
        self.post_office.append(post_officer)

        post_officer.send_message(WelcomeMessage(self.item_name))

        message = post_officer.receive_message()
        if isinstance(message, BidMessage):
            bid_value = message.value
            print(f"Received bid of {bid_value}.")
            self.bids.append((post_officer, bid_value))
        else:
            raise Exception("No bid received from client")

    def end_auction(self):
        ordered_bids = sorted(self.bids, key=lambda x: x[1])

        for post_officer, _ in ordered_bids[:-1]:
            post_officer.send_message(LosingMessage())

        post_officer, value = ordered_bids[-1]

        post_officer.send_message(BillMessage(value))

        print("Waiting for the payment...")
        message = post_officer.receive_message()

        if isinstance(message, PaymentMessage):
            print("Payment received :-)")
        else:
            raise Exception("No payment received from the client")

        print("Auction ended with sucess.")
        self.close_connections()

    def close_connections(self):
        for post_officer in self.post_office:
            post_officer.close()

        self.listener_socket.close()


if __name__ == "__main__":
    Server().run()
