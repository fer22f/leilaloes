from ssl import PROTOCOL_TLS_CLIENT, SSLContext

from client import Client
from config import CERTIFICATE_FILE, HOSTNAME


class SecureClient(Client):
    def __init__(self):
        context = SSLContext(PROTOCOL_TLS_CLIENT)
        context.load_verify_locations(CERTIFICATE_FILE)
        self.context = context
        self.hostname = HOSTNAME

    def create_socket(self):
        socket = self.create_simple_socket()
        return self.context.wrap_socket(socket, server_hostname=self.hostname)


if __name__ == "__main__":
    SecureClient().run()
