import json
from json.decoder import JSONDecodeError

from message import MESSAGES, Message


class PostOfficer:
    def __init__(self, socket, address):
        self.socket = socket
        self.address = address
        self.buffer = ""

    def close(self):
        self.socket.close()

    def send_message(self, message):
        self.send(message.serialize())

    def receive_message(self):
        try:
            message = self.receive()
        except (UnicodeDecodeError, JSONDecodeError):
            # If the message is not valid UTF-8, a UnicodeDecodeError will be raised.
            # If the message is malformed JSON, a JSONDecodeError will be raised.
            # In both cases, we return an empty message, since it is invalid.
            return Message()

        for message_cls in MESSAGES:
            if message["type"] == message_cls.type:
                # Deserialize message using keyword arguments in __init__
                return message_cls(**message["data"])
        return Message()

    def send(self, simple_object):
        # Appends the end of message delimiter
        message = json.dumps(simple_object) + "\n"
        self.socket.send(message.encode())

    def receive(self):
        received = self.socket.recv(1024)
        self.buffer += received.decode()
        try:
            # Tries to get the first message delimiter (multiple may exist)
            ix = self.buffer.index("\n")
        except ValueError:
            # If we have a partial message, no end delimiter will be found.
            # To solve that, we call receive again to wait for the rest of the message.
            return self.receive()
        message = self.buffer[:ix]
        self.buffer = self.buffer[ix + 1 :]
        return json.loads(message)
