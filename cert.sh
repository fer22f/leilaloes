#!/bin/bash

COUNTRY='BR'
STATE='Paraná'
LOCALITY='Curitiba'
ORGANIZATION='Leila Leilões LTDA'
ORGANIZATION_UNIT='Divisão de TI'
COMMON_NAME='leilaleiloes.onion'

CERTIFICATE_FILE='cert.pem'
PRIVATE_KEY_FILE='key.pem'

SUBJECT="/C=$COUNTRY"
SUBJECT+="/ST=$STATE"
SUBJECT+="/L=$LOCALITY"
SUBJECT+="/O=$ORGANIZATION"
SUBJECT+="/OU=$ORGANIZATION_UNIT"
SUBJECT+="/CN=$COMMON_NAME"

openssl req -new -x509 -days 365 -nodes \
    -out $CERTIFICATE_FILE \
    -keyout $PRIVATE_KEY_FILE \
    -subj "$SUBJECT"
