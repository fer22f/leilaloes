class Message:
    def __init__(self):
        self.data = dict()

    def serialize(self):
        message = {"type": self.type}
        message["data"] = self.data
        return message


class WelcomeMessage(Message):
    type = "Welcome"

    def __init__(self, item_name):
        super().__init__()
        self.item_name = item_name
        self.data["item_name"] = item_name


class BidMessage(Message):
    type = "Bid"

    def __init__(self, value):
        super().__init__()
        self.value = value
        self.data["value"] = value


class LosingMessage(Message):
    type = "Losing"


class BillMessage(Message):
    type = "Bill"

    def __init__(self, value):
        super().__init__()
        self.value = value
        self.data["value"] = value


class PaymentMessage(Message):
    type = "Payment"


MESSAGES = (WelcomeMessage, BidMessage, LosingMessage, BillMessage, PaymentMessage)
