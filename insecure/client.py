from socket import create_connection

from config import SERVER_ADDRESS
from message import BidMessage, BillMessage, LosingMessage, PaymentMessage, WelcomeMessage
from postofficer import PostOfficer


class Client:
    def create_simple_socket(self):
        return create_connection(SERVER_ADDRESS)

    def create_socket(self):
        return self.create_simple_socket()

    def run(self):
        self.post_officer = PostOfficer(self.create_socket(), SERVER_ADDRESS)

        self.enter_auction()
        self.do_bid()
        is_winner = self.receive_result()

        if is_winner:
            self.make_payment()

        self.post_officer.close()

    def enter_auction(self):
        message = self.post_officer.receive_message()
        if isinstance(message, WelcomeMessage):
            print(f"Auctioned item: {message.item_name}")
        else:
            raise Exception("No welcome message received from server")

    def do_bid(self):
        bid_value = int(input("Enter your bid: "))
        self.post_officer.send_message(BidMessage(bid_value))

    def receive_result(self):
        message = self.post_officer.receive_message()
        if isinstance(message, LosingMessage):
            print("We lost :-(")
            return False
        elif isinstance(message, BillMessage):
            print(f"We won, we will need to pay {message.value}.")
            return True
        else:
            raise Exception("Unexpected result message received from server")

    def make_payment(self):
        input("Press enter to make the payment.")
        self.post_officer.send_message(PaymentMessage())


if __name__ == "__main__":
    Client().run()
