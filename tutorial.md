# Leilalões - Testing TLS security properties by constructing a toy example, a simple auction application
> By Djenifer and Fernando

TLS promises to bring three security properties via a simple wrapper: Confidentiality (nobody can read your messages), Authenticity (you're talking to a known server) and Integrity (the messages can't be tampered with). We will start by constructing an insecure system, then proceeding to add TLS to it and in the end we will inspect the messages that it sends and try to mess with them.

So let's begin by constructing a simple, insecure, auctioning system. The server accepts connections in a given TCP port and informs the clients about the item that is being auctioned. After knowing what item is being auctioned, each client sends a bid, in a sealed-bid auction fashion. Once all clients send their bids, the one with the biggest offer wins, and is required to pay immediately. The code can be modified to work with second-price winning bids, which is often used instead.

## Basic insecure auction (no bids)

In order to start coding, we will need to first define the **protocol**. The first thing a client needs to know is which item is currently being auctioned, and conversely that is the first thing that the server should send.

We will create a `Message` class that can be serialized into JSON, which can then be sent over the wire. Our protocol will be composed of `Message`'s subclasses, and our first will be the `WelcomeMessage` which will contain the auctioned item name, and is the first message that a new client will receive from the server.

> insecure_basic/message.py
```python
class Message:
    def __init__(self):
        self.data = dict()

    def serialize(self):
        message = {"type": self.type}
        message["data"] = self.data
        return message


class WelcomeMessage(Message):
    type = "Welcome"

    def __init__(self, item_name):
        super().__init__()
        self.item_name = item_name
        self.data["item_name"] = item_name


MESSAGES = (WelcomeMessage,)
```

However, our sockets can only send and receive bytes, not dictionaries! So we will wrap them in a class that can send and receive messages, which will be called `PostOfficer`. Dealing with raw bytes is messy; we can receive partial messages and even multiple messages can end up queued up. We will solve this problem by using a newline as the end of message delimiter and keep a buffer to make sure that partial messages eventually end up being complete.

> insecure_basic/postofficer.py
```python
import json
from json.decoder import JSONDecodeError

from message import MESSAGES, Message


class PostOfficer:
    def __init__(self, socket, address):
        self.socket = socket
        self.address = address
        self.buffer = ""

    def close(self):
        self.socket.close()

    def send_message(self, message):
        self.send(message.serialize())

    def receive_message(self):
        try:
            message = self.receive()
        except (UnicodeDecodeError, JSONDecodeError):
            # If the message is not valid UTF-8, a UnicodeDecodeError will be raised.
            # If the message is malformed JSON, a JSONDecodeError will be raised.
            # In both cases, we return an empty message, since it is invalid.
            return Message()

        for message_cls in MESSAGES:
            if message["type"] == message_cls.type:
                # Deserialize message using keyword arguments in __init__
                return message_cls(**message["data"])
        return Message()

    def send(self, simple_object):
        # Appends the end of message delimiter
        message = json.dumps(simple_object) + "\n"
        self.socket.send(message.encode())

    def receive(self):
        received = self.socket.recv(1024)
        self.buffer += received.decode()
        try:
            # Tries to get the first message delimiter (multiple may exist)
            ix = self.buffer.index("\n")
        except ValueError:
            # If we have a partial message, no end delimiter will be found.
            # To solve that, we call receive again to wait for the rest of the message.
            return self.receive()
        message = self.buffer[:ix]
        self.buffer = self.buffer[ix + 1 :]
        return json.loads(message)
```

Now, we can start the client, which will create a socket to connect to the server and once it is connected, it will receive the `WelcomeMessage`.

> insecure_basic/client.py
```python
from socket import create_connection

from config import SERVER_ADDRESS
from message import WelcomeMessage
from postofficer import PostOfficer


class Client:
    def create_simple_socket(self):
        return create_connection(SERVER_ADDRESS)

    def create_socket(self):
        return self.create_simple_socket()

    def run(self):
        self.post_officer = PostOfficer(self.create_socket(), SERVER_ADDRESS)

        self.enter_auction()

        self.post_officer.close()

    def enter_auction(self):
        message = self.post_officer.receive_message()
        if isinstance(message, WelcomeMessage):
            print(f"Auctioned item: {message.item_name}")
        else:
            raise Exception("No welcome message received from server")


if __name__ == "__main__":
    Client().run()
```

Next we can create the server. In it, we first ask what is the name of the item to be auctioned, and then we need to do two things concurrently: Wait for the clients to arrive, and wait for a command to end the auction. To make that happen, we spawn a thread to wait for the clients using the `threading` module. Once a client arrives, we send a `WelcomeMessage` containing the auctioned item.

> insecure_basic/server.py
```python
from socket import create_server
from threading import Thread

from config import PORT
from message import WelcomeMessage
from postofficer import PostOfficer


class Server:
    def __init__(self):
        self.post_office = []
        self.listener_socket = None

    def run(self):
        self.item_name = input("What is the name of the item to be auctioned? ")
        Thread(target=self.wait_for_clients, args=()).start()

        print("Press enter to close all connections.")
        self.close_connections()

    def create_simple_socket(self):
        return create_server(("", PORT))

    def create_socket(self):
        return self.create_simple_socket()

    def wait_for_clients(self):
        self.listener_socket = self.create_socket()
        while True:
            try:
                socket, address = self.listener_socket.accept()
                Thread(
                    target=self.handle_client, args=(PostOfficer(socket, address),)
                ).start()
            except ConnectionAbortedError:
                # When the listener_socket is closed by the main thread
                break

    def handle_client(self, post_officer):
        self.post_office.append(post_officer)

        post_officer.send_message(WelcomeMessage(self.item_name))

    def close_connections(self):
        for post_officer in self.post_office:
            post_officer.close()

        self.listener_socket.close()


if __name__ == "__main__":
    Server().run()
```

With this, the server now welcomes the client with the auction item. Once we configure a port and a server address like so,

> insecure_basic/config.py
```python
PORT = 52475
SERVER_ADDRESS = ("localhost", PORT)
```

We can run this basic example, and it ends up looking something like this:

```
$ python3 ../insecure_basic/server.py
What is the name of the item to be auctioned? Italian Greyhound NFT
Press enter to close all connections.
```

```
$ python3 ./insecure_basic/client.py
Auctioned item: Italian Greyhound NFT
```

## Complete insecure auction

In order to make bids and payments work we just need to add a bit more logic. We need to add a few more messages, such as a `BidMessage` (sent from the client to make a bid on the item), a `LosingMessage` and `BillMessage` which correspond to losing and winning responses from the server and to end it all, a `PaymentMessage` sent by the client to confirm the winning transaction has been completed.

> insecure/message.py
```python
class Message:
    def __init__(self):
        self.data = dict()

    def serialize(self):
        message = {"type": self.type}
        message["data"] = self.data
        return message


class WelcomeMessage(Message):
    type = "Welcome"

    def __init__(self, item_name):
        super().__init__()
        self.item_name = item_name
        self.data["item_name"] = item_name


class BidMessage(Message):
    type = "Bid"

    def __init__(self, value):
        super().__init__()
        self.value = value
        self.data["value"] = value


class LosingMessage(Message):
    type = "Losing"


class BillMessage(Message):
    type = "Bill"

    def __init__(self, value):
        super().__init__()
        self.value = value
        self.data["value"] = value


class PaymentMessage(Message):
    type = "Payment"


MESSAGES = (WelcomeMessage, BidMessage, LosingMessage, BillMessage, PaymentMessage)
```

With this, we can now complete the client with the bidding and payment logic.

> insecure/client.py
```python
from socket import create_connection

from config import SERVER_ADDRESS
from message import BidMessage, BillMessage, LosingMessage, PaymentMessage, WelcomeMessage
from postofficer import PostOfficer


class Client:
    def create_simple_socket(self):
        return create_connection(SERVER_ADDRESS)

    def create_socket(self):
        return self.create_simple_socket()

    def run(self):
        self.post_officer = PostOfficer(self.create_socket(), SERVER_ADDRESS)

        self.enter_auction()
        self.do_bid()
        is_winner = self.receive_result()

        if is_winner:
            self.make_payment()

        self.post_officer.close()

    def enter_auction(self):
        message = self.post_officer.receive_message()
        if isinstance(message, WelcomeMessage):
            print(f"Auctioned item: {message.item_name}")
        else:
            raise Exception("No welcome message received from server")

    def do_bid(self):
        bid_value = int(input("Enter your bid: "))
        self.post_officer.send_message(BidMessage(bid_value))

    def receive_result(self):
        message = self.post_officer.receive_message()
        if isinstance(message, LosingMessage):
            print("We lost :-(")
            return False
        elif isinstance(message, BillMessage):
            print(f"We won, we will need to pay {message.value}.")
            return True
        else:
            raise Exception("Unexpected result message received from server")

    def make_payment(self):
        input("Press enter to make the payment.")
        self.post_officer.send_message(PaymentMessage())


if __name__ == "__main__":
    Client().run()
```

And also complete the server with the same logic. It should be noted that we don't solve the problem that in distributed systems literature is called "reliable multicast". What this means is that we don't make sure that all the clients are still alive when we send the result, which would be needed in an auction scenario to make sure that the winner is someone that is still connected and can pay the bill (if the winner somehow disconnects, another winner for the auction could be chosen). We don't handle disconnects in general, and once a client makes a bid, we assume it will stay connected until the end of the auction.

> insecure/server.py
```python
from socket import create_server
from threading import Thread

from config import PORT
from message import BidMessage, BillMessage, LosingMessage, PaymentMessage, WelcomeMessage
from postofficer import PostOfficer


class Server:
    def __init__(self):
        self.bids = []
        self.post_office = []
        self.listener_socket = None

    def run(self):
        self.item_name = input("What is the name of the item to be auctioned? ")
        Thread(target=self.wait_for_clients, args=()).start()

        print("Press enter to end the auction.")
        while True:
            input()
            if len(self.bids) > 0:
                break
            print("The auction needs at least one bid before it can be ended.")

        self.end_auction()

    def create_simple_socket(self):
        return create_server(("", PORT))

    def create_socket(self):
        return self.create_simple_socket()

    def wait_for_clients(self):
        self.listener_socket = self.create_socket()
        while True:
            try:
                socket, address = self.listener_socket.accept()
                Thread(
                    target=self.handle_client, args=(PostOfficer(socket, address),)
                ).start()
            except ConnectionAbortedError:
                # When the listener_socket is closed by the main thread
                break

    def handle_client(self, post_officer):
        self.post_office.append(post_officer)

        post_officer.send_message(WelcomeMessage(self.item_name))

        message = post_officer.receive_message()
        if isinstance(message, BidMessage):
            bid_value = message.value
            print(f"Received bid of {bid_value}.")
            self.bids.append((post_officer, bid_value))
        else:
            raise Exception("No bid received from client")

    def end_auction(self):
        ordered_bids = sorted(self.bids, key=lambda x: x[1])

        for post_officer, _ in ordered_bids[:-1]:
            post_officer.send_message(LosingMessage())

        post_officer, value = ordered_bids[-1]

        post_officer.send_message(BillMessage(value))

        print("Waiting for the payment...")
        message = post_officer.receive_message()

        if isinstance(message, PaymentMessage):
            print("Payment received :-)")
        else:
            raise Exception("No payment received from the client")

        print("Auction ended with sucess.")
        self.close_connections()

    def close_connections(self):
        for post_officer in self.post_office:
            post_officer.close()

        self.listener_socket.close()


if __name__ == "__main__":
    Server().run()
```

If we run, we now get the full auction as such:

```
$ python3 ./insecure/server.py
What is the name of the item to be auctioned? Italian Greyhound NFT
Press enter to end the auction.
Received bid of 20.
Received bid of 30.

Waiting for the payment...
Pyment received :-)
Auction ended with success.
```

```
$ python3 ./insecure/client.py
Auctioned item: Italian Greyhound NFT
Enter your bid: 20
We lost :-(
```

```
$ python3 ./insecure/client.py
Auctioned item: Italian Greyhound NFT
Enter your bid: 30
We won, we will need to pay 30.
Press enter to make the payment.
```

## Securing the auction with TLS

In order to get started, we need to get hold of a server certificate and its corresponding private key. Usually, you would get them by paying a certificate authority, but for our example it suffices that we create a self-signed certificate that is known by both the client and the server.

We can create a self-signed certificate and corresponding private key file by using `openssl`. Here is a script that generates a pair that is valid for a year:

> cert.sh
```bash
#!/bin/bash

COUNTRY='BR'
STATE='Paraná'
LOCALITY='Curitiba'
ORGANIZATION='Leila Leilões LTDA'
ORGANIZATION_UNIT='Divisão de TI'
COMMON_NAME='leilaleiloes.onion'

CERTIFICATE_FILE='cert.pem'
PRIVATE_KEY_FILE='key.pem'

SUBJECT="/C=$COUNTRY"
SUBJECT+="/ST=$STATE"
SUBJECT+="/L=$LOCALITY"
SUBJECT+="/O=$ORGANIZATION"
SUBJECT+="/OU=$ORGANIZATION_UNIT"
SUBJECT+="/CN=$COMMON_NAME"

openssl req -new -x509 -days 365 -nodes \
    -out $CERTIFICATE_FILE \
    -keyout $PRIVATE_KEY_FILE \
    -subj "$SUBJECT"
```

One important detail is that the common name (CN) of the certificate has to match the hostname property in the code. Usually, the hostname is related to a DNS record, as is the case with HTTP. Because of that, we now has a few more configurable properties:

> secure/config.py
```python
PORT = 52475
SERVER_ADDRESS = ("localhost", PORT)

CERTIFICATE_FILE = "cert.pem"
PRIVATE_KEY_FILE = "key.pem"
HOSTNAME = "leilaleiloes.onion"
```

Because the certificate is self-signed, it has to be available for both the client and the server. It should be noted that this is usually not the case when you get a certificate from a certificate authority, because their validity can be verified with the certificates already present in most computers. The private key however, needs to be available to the server, so that it can confirm its authenticity by proving it is the owner of the certificate.

Luckily, we made our server and client implementation easy to reuse, so all that is needed to get a secure implementation is subclassing our original server and client classes and adding some TLS code into it:

> secure/secure_server.py
```python
from ssl import PROTOCOL_TLS_SERVER, SSLContext

from config import CERTIFICATE_FILE, PRIVATE_KEY_FILE
from server import Server


class SecureServer(Server):
    def __init__(self):
        super().__init__()
        context = SSLContext(PROTOCOL_TLS_SERVER)
        context.load_cert_chain(CERTIFICATE_FILE, PRIVATE_KEY_FILE)
        self.context = context

    def create_socket(self):
        socket = self.create_simple_socket()
        return self.context.wrap_socket(socket, server_side=True)


if __name__ == "__main__":
    SecureServer().run()
```

> secure/secure_client.py
```python
from ssl import PROTOCOL_TLS_CLIENT, SSLContext

from client import Client
from config import CERTIFICATE_FILE, HOSTNAME


class SecureClient(Client):
    def __init__(self):
        context = SSLContext(PROTOCOL_TLS_CLIENT)
        context.load_verify_locations(CERTIFICATE_FILE)
        self.context = context
        self.hostname = HOSTNAME

    def create_socket(self):
        socket = self.create_simple_socket()
        return self.context.wrap_socket(socket, server_hostname=self.hostname)


if __name__ == "__main__":
    SecureClient().run()
```

Let's see how our secure implementation behaves:
```
$ ./cert.sh
Generating a 2048 bit RSA private key
....................+++
..................................+++
writing new private key to 'key.pem'
-----
$ python3 ./insecure/secure_server.py
What is the name of the item to be auctioned? Italian Greyhound NFT
Press enter to end the auction.
Received bid of 20.
Received bid of 30.

Waiting for the payment...
Pyment received :-)
Auction ended with success.
```

```
$ python3 ./insecure/secure_client.py
Auctioned item: Italian Greyhound NFT
Enter your bid: 20
We lost :-(
```

```
$ python3 ./insecure/secure_client.py
Auctioned item: Italian Greyhound NFT
Enter your bid: 30
We won, we will need to pay 30.
Press enter to make the payment.
```

## Testing TLS security

We now have a secure auction implementation. How can we be so sure of TLS's security guarentees? We can test them! But first, it would be nice if we could intercept the encrypted messages, being able to modify them as we wish. Using the TLS library in its socket form makes this impossible: All the details are hidden within the library which talks directly to the operating system using the socket's file descriptor. However, the TLS library also offers another option, usually reserved for async library creators to use, which is the Binary IO (BIO) interface. Using this interface, TLS writes encrypted data into an outgoing data buffer, and reads encrypted data from an incoming data buffer, which we can intercept. We will create a TLS inspector using this interface.

> inspectable/inspectable.py
```python
import time
from ssl import MemoryBIO, SSLEOFError, SSLWantReadError, SSLWantWriteError


def hexdump(b):
    n = 0
    while n < len(b):
        # Hex string
        s1 = " ".join(f"{i:02x}" for i in b[n : n + 16])
        # Extra space between groups of 8 hex values
        s1 = s1[0:23] + " " + s1[23:]
        # ASCII string, when not in range, output "."
        s2 = "".join(chr(i) if 32 <= i <= 127 else "." for i in b[n : n + 16])

        print(f"{n:08x}: {s1:<48}  {s2}")
        n += 16


class InspectableListenerSocket:
    def __init__(self, listener_socket, context, **kwargs):
        self.listener_socket = listener_socket
        self.context = context
        self.kwargs = kwargs

    def accept(self):
        socket, address = self.listener_socket.accept()
        socket = InspectableSocket(socket, self.context, **self.kwargs)
        socket.do_handshake()
        return socket, address

    def close(self):
        self.listener_socket.close()


class InspectableSocket:
    def __init__(self, socket, context, allow_editing=False, **kwargs):
        self.socket = socket
        # In the handshake, we need to read before writing, for reasons
        # that are explained further below. However, if the socket was blocking,
        # we would potentially get stuck, waiting for a message that never arrives.
        # So instead of that, we work with non-blocking sockets and their issues.
        self.socket.setblocking(False)
        self.context = context
        self.allow_editing = allow_editing
        self.incoming = MemoryBIO()
        self.outgoing = MemoryBIO()
        self.ssl_object = self.context.wrap_bio(self.incoming, self.outgoing, **kwargs)

    def handle_incoming(self, n_bytes=1024):
        try:
            data = self.socket.recv(n_bytes)
        except BlockingIOError:
            # Avoids a busy loop that consumes 100% of CPU resources
            time.sleep(0.01)
            return 0

        if len(data) == 0:
            # When recv returns an empty string, the socket has been closed
            return -1
        return self.incoming.write(data)

    def handle_outgoing(self):
        data = self.outgoing.read()
        if len(data) == 0:
            return
        print("========== send:  ssl ==========")
        hexdump(data)
        # We use sendall instead of send to make sure all data is sent
        self.socket.sendall(self.edit_data(data))

    def edit_data(self, original_data):
        if not self.allow_editing:
            return original_data
        answer = input("Do you want to edit this message? [y/N] ")
        if answer[:1].lower() == "y":
            index = int(input("Enter the index in hexadecimal: "), 16)
            byte = int(input("Enter the byte in hexadecimal: "), 16)
            data = original_data[:index] + bytes([byte]) + original_data[index + 1 :]
            print("========= send: edited =========")
            hexdump(data)
            return data
        return original_data

    def do_handshake(self):
        # The do_handshake method should be repeatedly called until
        # the handshake is complete. When it needs to write or read,
        # it raises their respective Errors. As per documentation,
        # before writing, we should check if there is something to read,
        # and before reading, we should check if there is something to write.
        while True:
            try:
                self.ssl_object.do_handshake()
                break
            except SSLWantWriteError:
                if self.handle_incoming() == -1:
                    raise SSLEOFError()
                self.handle_outgoing()
            except SSLWantReadError:
                self.handle_outgoing()
                if self.handle_incoming() == -1:
                    raise SSLEOFError()
        # Important to make sure that the handshake is completed
        self.handle_outgoing()
        print(f"Cipher: {self.ssl_object.cipher()}")

    def recv(self, n_bytes):
        while True:
            try:
                data = self.ssl_object.read(n_bytes)
                return data
            except SSLWantReadError:
                # This may happen if the SSL message has been chunked
                if self.handle_incoming() == -1:
                    return b""

    def send(self, data):
        print("======== send: original ========")
        hexdump(data)
        # There is no loop to send here; since TLS writes into a buffer
        # that resizes automatically, it is never expected to fail.
        self.ssl_object.write(data)
        self.handle_outgoing()

    def close(self):
        self.socket.close()
```

Now, we can create an inspectable server and an inspectable client, allowing for editing the sent packets if we wish.

> inspectable/inspectable_server.py
```python
from secure_server import SecureServer

from inspectable import InspectableListenerSocket


class InspectableSecureServer(SecureServer):
    def create_socket(self):
        socket = self.create_simple_socket()
        return InspectableListenerSocket(socket, self.context, server_side=True)


if __name__ == "__main__":
    InspectableSecureServer().run()
```

> inspectable/inspectable_client.py
```python
import sys

from secure_client import SecureClient

from inspectable import InspectableSocket


class InspectableSecureClient(SecureClient):
    def __init__(self, allow_editing=False):
        super().__init__()
        self.allow_editing = allow_editing

    def create_socket(self):
        socket = self.create_simple_socket()
        socket = InspectableSocket(
            socket,
            self.context,
            allow_editing=self.allow_editing,
            server_hostname=self.hostname,
        )
        socket.do_handshake()
        return socket


if __name__ == "__main__":
    allow_editing = "--edit" in sys.argv
    InspectableSecureClient(allow_editing=allow_editing).run()
```

Let's see all the encrypted data involved in a simple bid. Please note that we only show what encrypted data is **sent**. And don't be confused: The one that initiates the handshake is the client, as it sends the first message.

```
$ python3 ./inspectable/inspectable_server.py
What is the name of the item to be auctioned? Italian Greyhound NFT
Press enter to end the auction.
========== send:  ssl ==========
00000000: 16 03 03 00 7a 02 00 00  76 03 03 99 52 33 65 57  ....z...v...R3eW
00000010: 8f 53 9f b8 d0 34 2b 42  e5 a3 60 8f 99 da e3 f3  .S...4+B..`.....
00000020: d9 07 73 7f 35 9a 8f b9  03 49 4b 20 8f 41 ea ea  ..s5....IK .A..
00000030: 6d 33 b5 37 5e 04 fa ed  0b 6b 89 8c 44 3d 01 4e  m3.7^....k..D=.N
00000040: d6 24 3d e4 18 a7 25 32  92 e1 b9 90 13 02 00 00  .$=...%2........
00000050: 2e 00 2b 00 02 03 04 00  33 00 24 00 1d 00 20 40  ..+.....3.$... @
00000060: 35 4e 84 05 37 92 2f af  a2 a1 d4 42 52 09 25 be  5N..7./....BR.%.
00000070: 8a 80 4e 23 75 e5 18 20  d1 5b b0 d8 22 f9 4f 14  ..N#u.. .[..".O.
00000080: 03 03 00 01 01 17 03 03  00 17 ac 06 75 06 3c b2  ............u.<.
00000090: 2d d2 de 7f b4 16 ba e5  ce f9 b3 2e d1 d4 13 9e  -..............
000000a0: 33 17 03 03 03 b8 92 eb  eb 50 66 f9 ad 02 02 71  3........Pf....q
000000b0: 5a 1a ac 17 2b 2d 18 3f  81 d5 96 43 56 c4 9c 81  Z...+-.?...CV...
000000c0: d1 e9 e3 c4 ae d7 82 09  8c 1d 52 0f 8c ba 66 c9  ..........R...f.
000000d0: 06 31 53 cf a4 8f 0b b2  87 a3 44 b2 b3 4a d4 50  .1S.......D..J.P
000000e0: 56 68 ba 70 99 c4 90 de  dc 63 9b 38 34 47 e3 6d  Vh.p.....c.84G.m
000000f0: c7 ae 75 86 51 c1 b7 d3  33 bc fb 3c 92 fa b7 ce  ..u.Q...3..<....
00000100: 56 b6 d8 81 39 aa 6c ac  02 d2 c9 fc 69 dd b8 99  V...9.l.....i...
00000110: 1a 47 42 ef e0 01 ee 4e  30 f2 8e c2 b4 98 a7 63  .GB....N0......c
00000120: 66 24 6e 6d e2 99 11 6f  a2 52 80 e7 1f 88 42 e6  f$nm...o.R....B.
00000130: 58 3a 81 a4 a9 46 74 3f  93 57 a8 e5 7a 9d 56 fb  X:...Ft?.W..z.V.
00000140: 0d b8 ac 38 d3 14 0d c5  e0 c0 93 31 8f 2f f3 47  ...8.......1./.G
00000150: ff f7 ec 32 4c 62 2e 8f  5b 60 39 c7 48 0b cd 7b  ...2Lb..[`9.H..{
00000160: 89 44 0a 41 f8 3c 71 d3  9f 59 7a 6b dd 99 b4 1a  .D.A.<q..Yzk....
00000170: 85 e0 6c 6b 58 51 29 68  67 46 5c 2b b6 6c 95 02  ..lkXQ)hgF\+.l..
00000180: c9 07 0e 58 1e b5 51 20  b3 47 19 90 13 79 af 0b  ...X..Q .G...y..
00000190: fc a1 0b 9b 40 1f 24 f7  b3 4e d9 84 43 0c e6 53  ....@.$..N..C..S
000001a0: fd 01 ae 71 f4 8c b7 8a  21 5e 91 f2 e8 29 1b dc  ...q....!^...)..
000001b0: 6b c6 42 8a b6 45 18 18  03 37 18 4b ff 32 89 c2  k.B..E...7.K.2..
000001c0: fa ce 3c b9 eb 11 9c e3  2c bc ac ee 1d 4d 24 d5  ..<.....,....M$.
000001d0: ed 94 26 3d f4 ac 54 15  61 2c d9 cc c2 0c 82 ae  ..&=..T.a,......
000001e0: 79 d9 12 4e 85 34 72 a3  4f fe ad 2d a1 15 9b 94  y..N.4r.O..-....
000001f0: 9b f1 62 bb 7b 18 97 6d  b7 db ab d6 08 c4 ee 28  ..b.{..m.......(
00000200: 33 de a4 15 06 75 df d9  8c 5a f1 d8 32 db e6 16  3....u...Z..2...
00000210: 08 bd b2 99 bf 82 a3 83  8c 05 d5 29 5b ee 61 f7  ...........)[.a.
00000220: 1b 66 99 f3 c2 48 16 65  80 78 6c f7 db 05 d7 8a  .f...H.e.xl.....
00000230: 38 c5 95 67 24 3e e9 1d  18 37 ce 69 a0 4a bc e4  8..g$>...7.i.J..
00000240: 18 2e 62 5d 08 00 89 88  6b fc ec bb c4 c4 58 4c  ..b]....k.....XL
00000250: 93 59 93 98 80 e7 89 a0  84 63 45 dd 2e d5 93 eb  .Y.......cE.....
00000260: 87 cb f1 0a 73 b4 0a bb  48 97 74 3b 59 5f dc 69  ....s...H.t;Y_.i
00000270: f6 1e 5b 56 fd 02 4d 26  09 20 46 cf 59 29 3c 9e  ..[V..M&. F.Y)<.
00000280: 33 b3 a9 86 64 fa 1c b9  af eb 2d f0 ba c0 02 68  3...d.....-....h
00000290: 09 a2 9e 75 33 e2 c5 26  ef ab 87 e4 16 26 cf 3f  ...u3..&.....&.?
000002a0: b0 50 6c d6 0f ba a2 06  ce ae 4e fb 71 10 57 9f  .Pl.......N.q.W.
000002b0: 0b 65 4e e8 aa 45 10 fa  43 e7 03 98 e8 82 c2 e2  .eN..E..C.......
000002c0: d8 70 97 29 cb b5 a0 99  50 77 2d 7b dc a9 aa 47  .p.)....Pw-{...G
000002d0: 1a 0b be 31 05 d7 bc a9  4d 40 83 7b ca 5a 85 d7  ...1....M@.{.Z..
000002e0: 84 da b0 2c 5c 9f ef fc  1c c6 86 71 3e d5 e1 ac  ...,\......q>...
000002f0: 7b ba 44 69 c9 ed 8e ee  c5 dd 99 35 88 e3 0d 48  {.Di.......5...H
00000300: 3d b4 ec 04 bc 91 4b f3  d2 56 ed ec 88 c3 cc 8b  =.....K..V......
00000310: 44 63 4c 2d 26 a9 64 b9  69 cc 21 82 f2 4d 3f 77  DcL-&.d.i.!..M?w
00000320: 70 f2 c7 03 30 d7 19 0d  92 2f a3 12 01 39 fd 4b  p...0..../...9.K
00000330: 41 42 c6 30 2c eb 94 cd  7c e0 fd 93 1c 6a c9 87  AB.0,...|....j..
00000340: bb a5 72 a3 67 d4 1c c2  86 6f cc bc 65 a8 ef f9  ..r.g....o..e...
00000350: df da 20 ae c9 fe d0 3f  af a6 b4 2f 47 fd 98 7c  .. ....?.../G..|
00000360: 84 c1 3e 27 44 69 a0 25  cb ec 33 d3 27 c0 2c c5  ..>'Di.%..3.'.,.
00000370: 3c 15 d4 fb de 16 39 52  8c 38 18 1b 23 7c 35 f9  <.....9R.8..#|5.
00000380: e1 e6 e5 ba 6c 63 3a c5  e8 37 1f d8 17 94 b5 a2  ....lc:..7......
00000390: 80 20 ba 57 19 59 5b 3c  67 9c 4a e1 19 70 0d 4d  . .W.Y[<g.J..p.M
000003a0: 5a c2 c7 f3 15 68 d2 19  73 33 24 16 9a 6f c0 da  Z....h..s3$..o..
000003b0: d3 3d 72 75 a7 4b bf 6c  92 cc 8b c7 be e4 91 32  .=ru.K.l.......2
000003c0: 72 e7 5a 9b ce e4 91 cb  8d c1 fc 94 07 3a 4e c4  r.Z..........:N.
000003d0: d9 39 8a 39 97 e9 2c 54  60 c9 36 35 3e bb 7e 23  .9.9..,T`.65>.~#
000003e0: 17 1b 77 84 fe cf 92 31  e9 10 9f df c5 fa 88 8b  ..w....1........
000003f0: 8c 68 6e 58 83 ab 91 ca  0e c6 45 5d 95 1a 89 eb  .hnX......E]....
00000400: 91 2f d3 9d 63 eb 33 f7  8d 1a fc 1e 18 b0 f9 ad  ./..c.3.........
00000410: aa 57 33 11 63 fd 7e 85  f7 6b 0a c0 78 30 f9 55  .W3.c.~..k..x0.U
00000420: 7a 46 07 d7 2f 68 db 36  2a b9 c1 ea ba cc cd 2e  zF../h.6*.......
00000430: 69 cb db 9d 44 b3 26 73  d2 a4 5e 92 fb 70 d1 3b  i...D.&s..^..p.;
00000440: 39 97 52 54 45 ee e4 91  3e 3b 59 95 df 85 63 82  9.RTE...>;Y...c.
00000450: 71 6b 9f 1a ba 3b a3 28  8d 1e 11 44 94 31 17 03  qk...;.(...D.1..
00000460: 03 01 19 f3 74 29 59 27  32 7c 62 88 0d 30 a9 06  ....t)Y'2|b..0..
00000470: 1e 0c 93 b3 93 03 03 18  d8 18 5c 6e 05 85 3d ce  ..........\n..=.
00000480: bd 7d a0 4d 72 36 1d 3a  c4 8c 17 7f 3e cd ed a7  .}.Mr6.:...>...
00000490: b8 50 db e7 d9 c3 12 5d  33 56 ea d9 28 72 36 12  .P.....]3V..(r6.
000004a0: 9f ac 2c cf 94 4f 2a 94  84 af c3 57 60 78 82 5b  ..,..O*....W`x.[
000004b0: e7 73 7d f1 76 15 6a 0b  63 94 de b8 98 3f 88 d4  .s}.v.j.c....?..
000004c0: 6c 70 78 3c 8e 9c 8b 0a  b0 03 bb 20 0f ba 14 37  lpx<....... ...7
000004d0: 28 66 10 6b ff 77 38 2d  13 8d b3 91 39 1f d0 ca  (f.k.w8-....9...
000004e0: 8b 85 01 23 e4 15 5c c7  fa ce e3 e9 a1 b8 b2 10  ...#..\.........
000004f0: cb 82 c3 7b 0b ea 0d 36  73 5d aa 9e 57 27 5f ad  ...{...6s]..W'_.
00000500: 07 e1 f5 79 4a 2b 2c 20  20 c3 f0 b5 d3 cc 79 c8  ...yJ+,  .....y.
00000510: 7d 8b f2 04 f5 6b 6f 4b  39 e7 b0 ca 78 da 11 bd  }....koK9...x...
00000520: 83 c3 a3 c9 1a 33 ef 76  03 53 f9 4f ab 56 0f 17  .....3.v.S.O.V..
00000530: f8 c9 83 58 bf a8 3c f6  dd 9d 03 51 a2 f3 76 30  ...X..<....Q..v0
00000540: e3 98 4f 02 44 ef f4 e7  8a a5 2f 02 63 67 92 4a  ..O.D...../.cg.J
00000550: 7c 2f 88 d9 4c 05 6c 1c  95 96 e8 b3 27 bd 9f 0d  |/..L.l.....'...
00000560: ec dd 51 16 79 7b ab 27  ce 26 90 e7 6c b1 37 fa  ..Q.y{.'.&..l.7.
00000570: 53 36 09 54 12 76 2f 1d  ad 89 a4 11 17 03 03 00  S6.T.v/.........
00000580: 45 03 35 d5 f1 87 7c cd  ec 0b 1a 50 a0 75 0f a4  E.5...|....P.u..
00000590: 3f 7d 45 c6 d8 c5 38 cd  6f 6a c9 2d b1 67 9f 2f  ?}E...8.oj.-.g./
000005a0: b3 7a c8 a7 69 e3 3a 06  06 0e 56 c9 02 da 10 1c  .z..i.:...V.....
000005b0: 91 66 bf b9 87 b2 6c 9f  0c 15 17 3e 7f f6 9e 92  .f....l....>...
000005c0: 66 85 94 c5 c6 0f                                 f.....
========== send:  ssl ==========
00000000: 17 03 03 00 fa ea e7 97  17 4b 1b 69 52 5c 0c 5d  .........K.iR\.]
00000010: 7f 2f d8 58 81 85 42 8c  fd de 52 7a df 7d 6e 31  /.X..B...Rz.}n1
00000020: ce 0f 27 e2 47 6c e8 05  17 e0 b6 25 8a c9 64 d6  ..'.Gl.....%..d.
00000030: a7 cc 3b ea b2 a8 ce 3a  be ae 24 c8 19 af 2c c9  ..;....:..$...,.
00000040: fa 12 c7 2a ec 7c ce 59  10 74 ba fc 9f 01 38 96  ...*.|.Y.t....8.
00000050: 5f 9e c3 04 73 ab 68 9e  df b4 c2 bd c7 de 51 54  _...s.h.......QT
00000060: be 67 e9 77 61 5f 28 00  ae 84 6f 80 2c 48 03 21  .g.wa_(...o.,H.!
00000070: 87 08 03 05 88 92 93 35  33 ce a7 75 49 35 5d 5a  .......53..uI5]Z
00000080: 2e 70 a8 39 c5 3d a5 3e  1b cd 23 27 d1 ef a0 e3  .p.9.=.>..#'....
00000090: 93 25 fb bd 49 64 9b 18  ef f0 40 c2 a0 d9 6b ee  .%..Id....@...k.
000000a0: 2f 55 15 c7 fb d4 4c bf  b6 64 ad 07 42 aa e8 9d  /U....L..d..B...
000000b0: 12 82 80 6b a4 ce 9d b4  92 04 46 7b 45 60 bc 0f  ...k......F{E`..
000000c0: aa 3c 0c ea 4a dc a6 90  1d c0 3b 73 f3 ce b3 ba  .<..J.....;s....
000000d0: 4b ca f8 27 9e 7f 25 1a  46 d7 fe 5d c6 79 62 63  K..'.%.F..].ybc
000000e0: cf fd 8e f3 85 83 c8 5d  31 cf 44 25 f7 cf 2a 3d  .......]1.D%..*=
000000f0: 0e 0f 58 0c 86 8e 94 29  7d af ce 82 fa f7 23 17  ..X....)}.....#.
00000100: 03 03 00 fa b0 08 8f 66  b3 fc 73 eb 17 cf 4f 52  .......f..s...OR
00000110: 0f e6 59 b2 0f 86 6a 72  f5 45 88 2e 65 b8 78 f8  ..Y...jr.E..e.x.
00000120: 09 88 9d da cd 6c ab 45  e7 83 90 8b 3c 4d bd 92  .....l.E....<M..
00000130: 2e 1f e1 08 a7 b7 e5 69  6e 9b e2 d8 9a fb 71 71  .......in.....qq
00000140: e5 5b 89 00 6b 3c f2 a8  ad cf 4c 2d cc 27 c1 d4  .[..k<....L-.'..
00000150: 40 67 e7 72 45 cf 80 af  cb 27 5d 3a 4a 71 85 ba  @g.rE....']:Jq..
00000160: c7 09 69 dc 3c 3b 49 8d  99 87 2d 9d 16 54 56 b3  ..i.<;I...-..TV.
00000170: 2e 96 ee cc 07 5a 79 c6  cd bb 29 12 5a 0e f2 1f  .....Zy...).Z...
00000180: 96 89 bb bf d2 d9 7e 21  9d 1b ff d5 e9 04 b6 78  ......~!.......x
00000190: 9a 83 47 0a 22 57 e9 e1  ce 54 33 01 f7 0c 4b 37  ..G."W...T3...K7
000001a0: a3 0c ac 09 7f 2b c8 5f  21 29 be 90 ec 7b f9 56  ....+._!)...{.V
000001b0: f0 98 3e 1d 79 83 aa 6d  28 ac 65 6c c5 6e 30 b8  ..>.y..m(.el.n0.
000001c0: a9 53 76 25 93 3e 10 af  52 8f 8a 82 60 93 19 d3  .Sv%.>..R...`...
000001d0: fa b6 6b ff 9d e7 02 6c  f5 b7 61 4f 41 f0 41 ed  ..k....l..aOA.A.
000001e0: 52 6a 68 8c 3e 05 96 65  8d c1 b7 47 c4 b9 b2 30  Rjh.>..e...G...0
000001f0: f3 1e 6d ce 3c 97 1a 80  ba 25 82 0d 27 94        ..m.<....%..'.
Cipher: ('TLS_AES_256_GCM_SHA384', 'TLSv1.3', 256)
======== send: original ========
00000000: 7b 22 74 79 70 65 22 3a  20 22 57 65 6c 63 6f 6d  {"type": "Welcom
00000010: 65 22 2c 20 22 64 61 74  61 22 3a 20 7b 22 69 74  e", "data": {"it
00000020: 65 6d 5f 6e 61 6d 65 22  3a 20 22 49 74 61 6c 69  em_name": "Itali
00000030: 61 6e 20 47 72 65 79 68  6f 75 6e 64 20 4e 46 54  an Greyhound NFT
00000040: 22 7d 7d 0a                                       "}}.
========== send:  ssl ==========
00000000: 17 03 03 00 55 fe 21 a0  10 8d 95 c6 e0 7c db 02  ....U.!......|..
00000010: bf 44 c8 6a 4a de 66 81  1f 82 5c 26 58 dc 56 03  .D.jJ.f...\&X.V.
00000020: 76 b1 5a 34 08 17 e2 e0  40 5e 1f f6 f3 0b 46 52  v.Z4....@^....FR
00000030: 5b 15 d7 61 1c 67 ff 2b  6d 1a a1 20 0d 6e ab 0c  [..a.g.+m.. .n..
00000040: a5 55 9e 1d f0 42 4e 24  aa 2d 10 4c cc 5f c6 2a  .U...BN$.-.L._.*
00000050: 67 ad 03 42 74 31 bc 52  5d 80                    g..Bt1.R].
Received bid of 10.

======== send: original ========
00000000: 7b 22 74 79 70 65 22 3a  20 22 42 69 6c 6c 22 2c  {"type": "Bill",
00000010: 20 22 64 61 74 61 22 3a  20 7b 22 76 61 6c 75 65   "data": {"value
00000020: 22 3a 20 31 30 7d 7d 0a                           ": 10}}.
========== send:  ssl ==========
00000000: 17 03 03 00 39 8e 96 ab  4f da 6c 91 85 48 f0 4f  ....9...O.l..H.O
00000010: 88 8b 75 2c 27 2e 3c fb  99 45 90 19 2a 8e 92 b1  ..u,'.<..E..*...
00000020: ab ef 89 9f 4e ab 99 24  a7 10 dc 94 ef ba 13 80  ....N..$........
00000030: cc 20 02 1e 39 2a 7c 1d  4a ec e2 da 64 31        . ..9*|.J...d1
Waiting for the payment...
Payment received :-)
Auction ended with sucess.
```

```
$ python3 ./inspectable/inspectable_client.py
========== send:  ssl ==========
00000000: 16 03 01 02 00 01 00 01  fc 03 03 c7 00 45 93 d2  .............E..
00000010: 6a d6 5c dd bb 78 b9 16  e2 b7 a1 ce d7 7a 31 1a  j.\..x.......z1.
00000020: b8 7f 75 71 2d 98 aa 81  4e 62 48 20 8f 41 ea ea  .uq-...NbH .A..
00000030: 6d 33 b5 37 5e 04 fa ed  0b 6b 89 8c 44 3d 01 4e  m3.7^....k..D=.N
00000040: d6 24 3d e4 18 a7 25 32  92 e1 b9 90 00 3e 13 02  .$=...%2.....>..
00000050: 13 03 13 01 c0 2c c0 30  00 9f cc a9 cc a8 cc aa  .....,.0........
00000060: c0 2b c0 2f 00 9e c0 24  c0 28 00 6b c0 23 c0 27  .+./...$.(.k.#.'
00000070: 00 67 c0 0a c0 14 00 39  c0 09 c0 13 00 33 00 9d  .g.....9.....3..
00000080: 00 9c 00 3d 00 3c 00 35  00 2f 00 ff 01 00 01 75  ...=.<.5./.....u
00000090: 00 00 00 17 00 15 00 00  12 6c 65 69 6c 61 6c 65  .........leilale
000000a0: 69 6c 6f 65 73 2e 6f 6e  69 6f 6e 00 0b 00 04 03  iloes.onion.....
000000b0: 00 01 02 00 0a 00 0c 00  0a 00 1d 00 17 00 1e 00  ................
000000c0: 19 00 18 00 23 00 00 00  16 00 00 00 17 00 00 00  ....#...........
000000d0: 0d 00 30 00 2e 04 03 05  03 06 03 08 07 08 08 08  ..0.............
000000e0: 09 08 0a 08 0b 08 04 08  05 08 06 04 01 05 01 06  ................
000000f0: 01 03 03 02 03 03 01 02  01 03 02 02 02 04 02 05  ................
00000100: 02 06 02 00 2b 00 09 08  03 04 03 03 03 02 03 01  ....+...........
00000110: 00 2d 00 02 01 01 00 33  00 26 00 24 00 1d 00 20  .-.....3.&.$...
00000120: de a1 e1 1f ce 37 84 21  4a cd e5 93 a1 0e 0a a2  .....7.!J.......
00000130: 15 70 0b f5 8b aa 48 00  1c 65 d5 08 49 bc c2 0a  .p....H..e..I...
00000140: 00 15 00 c1 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000150: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000160: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000170: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000180: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000190: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001a0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001b0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001c0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001d0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001e0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001f0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000200: 00 00 00 00 00                                    .....
========== send:  ssl ==========
00000000: 14 03 03 00 01 01 17 03  03 00 45 e6 a4 f3 5f 89  ..........E..._.
00000010: 14 b5 36 6a 6a 28 7c 9f  82 81 4d cd 12 f8 0f 2b  ..6jj(|...M....+
00000020: a7 56 13 f0 32 45 31 e7  e5 17 98 52 57 9d 50 80  .V..2E1....RW.P.
00000030: d1 67 0c 88 21 6f ac 8b  b5 1c 63 d6 fd 32 be 25  .g..!o....c..2.%
00000040: 59 cc 29 e8 e6 2a 56 d9  5b 6f b4 2e 6c 43 94 ce  Y.)..*V.[o..lC..
Cipher: ('TLS_AES_256_GCM_SHA384', 'TLSv1.3', 256)
Auctioned item: Italian Greyhound NFT
Enter your bid: 10
======== send: original ========
00000000: 7b 22 74 79 70 65 22 3a  20 22 42 69 64 22 2c 20  {"type": "Bid",
00000010: 22 64 61 74 61 22 3a 20  7b 22 76 61 6c 75 65 22  "data": {"value"
00000020: 3a 20 31 30 7d 7d 0a                              : 10}}.
========== send:  ssl ==========
00000000: 17 03 03 00 38 00 b5 36  ae b7 e4 a2 de c2 cf 7f  ....8..6.......
00000010: 10 2e 27 f1 fc 3c 72 2d  57 4c 46 9f 8d e3 ac 04  ..'..<r-WLF.....
00000020: df 28 54 cd f7 0f a3 a1  0f 77 20 72 87 4c 04 76  .(T......w r.L.v
00000030: fa c4 3e 2f 56 30 fa 12  c4 a1 e2 19 96           ..>/V0.......
We won, we will need to pay 10.
Press enter to make the payment.
======== send: original ========
00000000: 7b 22 74 79 70 65 22 3a  20 22 50 61 79 6d 65 6e  {"type": "Paymen
00000010: 74 22 2c 20 22 64 61 74  61 22 3a 20 7b 7d 7d 0a  t", "data": {}}.
========== send:  ssl ==========
00000000: 17 03 03 00 31 69 68 5c  e9 60 23 18 85 69 9e 76  ....1ih\.`#..i.v
00000010: fd 51 f2 9e 97 03 9c 03  ab 02 6f 00 2e ac 52 5f  .Q........o...R_
00000020: f0 13 17 d2 08 c3 06 40  a3 4f 5c fe 45 66 88 a1  .......@.O\.Ef..
00000030: c5 6f ef 1b f9 da                                 .o....
```

We can see that the messages are indecipherable, and that the cipher used to
encode the messages is AEAD Advanced Encryption Standard with 256bit key in
Galois/Counter mode (AES 256 GCM) and the hash function used is the Secure Hash
Algorithm 384 (SHA384). Since both were run in modern Python, the version 1.3
of TLS has also been used, with 256 secret bits.

What we can also test is that the certificates are being checked. Since our
implementation reads the certificates from the current directory, we can create
another set of certificates in another directory and test if the client can
connect to a server with a certificate it doesn't know.

```
$ mkdir fake_certs
$ cd fake_certs
$ ../cert.sh
Generating a 2048 bit RSA private key
.................................+++
........................................+++
writing new private key to 'key.pem'
-----
$ python3 ../inspectable/inspectable_server.py
What is the name of the item to be auctioned? Italian Greyhound NFT
Press enter to end the auction.
========== send:  ssl ==========
00000000: 16 03 03 00 7a 02 00 00  76 03 03 3f b7 a0 21 9f  ....z...v..?..!.
00000010: 70 0c a0 f9 41 7a 70 2a  ca c4 44 4f fe 84 35 85  p...Azp*..DO..5.
00000020: a1 a7 34 28 5d d0 8e 9b  18 4a ca 20 47 6c ab 1f  ..4(]....J. Gl..
00000030: 48 46 63 f4 8a ee 52 ea  e0 0b 2d 1d 19 fc 31 fb  HFc...R...-...1.
00000040: 02 e1 47 ed 94 bf 8c c6  c5 71 3b bd 13 02 00 00  ..G......q;.....
00000050: 2e 00 2b 00 02 03 04 00  33 00 24 00 1d 00 20 b0  ..+.....3.$... .
00000060: 14 36 81 58 86 89 7f 52  a1 b2 1b 9d cb 92 61 02  .6.X..R......a.
00000070: de fe 5a e6 c0 c6 78 32  70 b7 56 0a 75 fd 07 14  ..Z...x2p.V.u...
00000080: 03 03 00 01 01 17 03 03  00 17 bd fa 6f b9 6d b6  ............o.m.
00000090: 46 bc 9e a7 46 5a da d4  4e b5 ca b3 86 4f 34 79  F...FZ..N....O4y
000000a0: e9 17 03 03 03 b8 78 7d  c0 24 e9 fc e1 6a f3 c7  ......x}.$...j..
000000b0: f1 54 99 fd 9a 07 5c 5e  bd 89 fd d1 1d 37 33 5e  .T....\^.....73^
000000c0: a2 fa 08 cc 9f b2 97 25  0e 97 1c 5c 02 72 90 2a  .......%...\.r.*
000000d0: 4d ed c3 b8 83 78 71 79  b0 6e 21 4f 32 b7 76 ec  M....xqy.n!O2.v.
000000e0: bb 65 26 14 85 30 ee a7  a8 8b ab 87 3f 8c 62 de  .e&..0......?.b.
000000f0: 3e f3 e0 03 0b 07 bd be  24 9b 40 df fa 1d ca 9b  >.......$.@.....
00000100: 3b 13 98 e3 97 21 09 94  65 7f 40 9f 3f 2e 55 4b  ;....!..e@.?.UK
00000110: bc d2 9b ff 80 23 c6 90  42 80 6e bc 72 81 9a 3a  .....#..B.n.r..:
00000120: 34 df 55 62 c1 5c dc cc  0b 38 bb d4 a8 f4 94 56  4.Ub.\...8.....V
00000130: 3c 8f 6e 49 11 94 ce 19  94 4d 57 9b 81 d9 3b 94  <.nI.....MW...;.
00000140: ac 29 03 b2 14 d6 86 ba  69 a2 ad 40 8f 6a 2b bf  .)......i..@.j+.
00000150: 6b 5a 5e d3 16 63 0c 46  c2 31 2f 4a 84 bd 91 cb  kZ^..c.F.1/J....
00000160: 0d 20 99 c8 4b f6 2e a3  da b5 8e b2 01 0d a2 6f  . ..K..........o
00000170: 11 d8 6e 38 66 3a 99 6d  c8 03 1e a3 63 bd 1e 87  ..n8f:.m....c...
00000180: a8 60 50 94 20 25 95 6e  16 3d 80 c0 bb 4d 61 02  .`P. %.n.=...Ma.
00000190: 8e 24 08 b5 4f 86 e3 eb  e2 d8 aa ae b2 8c 08 22  .$..O.........."
000001a0: ae 07 c2 f7 80 d8 63 65  d1 4a 58 c7 40 31 47 f7  ......ce.JX.@1G.
000001b0: 9c ce 09 4f 9c 03 b4 a0  98 2d fb b0 2d 01 17 1c  ...O.....-..-...
000001c0: 06 fd 65 66 bf a4 ef 08  40 05 5c c8 75 17 66 bf  ..ef....@.\.u.f.
000001d0: c5 99 15 6c bb 01 49 ab  2c f8 a0 03 ae b5 cc 28  ...l..I.,......(
000001e0: a4 4c 2b 9f 43 b3 0a 11  c4 50 2d ca 8f f9 13 f3  .L+.C....P-.....
000001f0: 7a 91 11 e5 8e c1 c4 55  9a 3d b7 c7 9e 17 52 1f  z......U.=....R.
00000200: 73 95 04 f8 09 14 99 93  40 0d 66 41 92 71 86 4f  s.......@.fA.q.O
00000210: 85 fc c7 33 ea ad 21 ae  9b d8 69 10 8d 72 b0 4c  ...3..!...i..r.L
00000220: 14 91 d0 fe a5 af 1e 8e  a8 5b 76 f2 ff 07 f8 84  .........[v.....
00000230: 8b d1 f0 d5 50 84 84 b9  67 61 02 99 d1 a4 61 af  ....P...ga....a.
00000240: f7 4d 78 30 c0 82 85 68  23 4c cc 2d 0b 02 09 55  .Mx0...h#L.-...U
00000250: 23 de ff 04 83 10 65 cc  01 ee 90 6b 2d 43 84 30  #.....e....k-C.0
00000260: d7 04 ed 67 67 8a 2e a6  f1 d9 09 e6 41 c2 aa 53  ...gg.......A..S
00000270: 5e 6d d9 6c 7b e5 41 33  65 1f 3c d0 de 19 7e 49  ^m.l{.A3e.<...~I
00000280: fd 73 38 07 0f fb 46 74  06 7d 75 3c c9 0c 00 6e  .s8...Ft.}u<...n
00000290: 3a 62 d2 ee 8a d3 9f 55  38 b2 cf 09 88 31 3d 97  :b.....U8....1=.
000002a0: 98 de 46 7f d5 ec 2a f4  af 9a d4 97 52 31 d2 8a  ..F..*.....R1..
000002b0: 5e 30 53 e4 38 e3 44 1c  0a 70 57 62 f9 70 84 9f  ^0S.8.D..pWb.p..
000002c0: 91 84 dc 28 27 cc 34 14  19 23 08 8e 6f 18 07 ac  ...('.4..#..o...
000002d0: b2 9a c6 bc 95 39 ae 3a  a7 8e d5 bd a6 2c 07 48  .....9.:.....,.H
000002e0: a3 0e d0 07 c8 32 31 da  da bd ff 53 67 90 e5 a6  .....21....Sg...
000002f0: b1 2d 51 a4 46 44 ee 82  05 f7 e2 c4 df bf 1e b8  .-Q.FD..........
00000300: 65 f6 86 48 cd fa af e7  ae 96 4d 45 fb 8b 7f ed  e..H......ME...
00000310: 88 01 0c 55 c8 34 a5 bf  00 f4 e0 1b 7b 51 0e fa  ...U.4......{Q..
00000320: 08 69 bc ca 44 7c 4d 0a  02 84 9c b1 09 ae 47 3a  .i..D|M.......G:
00000330: 39 44 18 50 71 31 72 c6  81 02 af 36 5e 5c d7 29  9D.Pq1r....6^\.)
00000340: 2c dc f3 ed ef 4b f2 96  f2 9b d7 91 41 f7 e1 9b  ,....K......A...
00000350: 94 38 b5 87 e2 6b 66 93  b5 e5 91 71 c5 11 0c 94  .8...kf....q....
00000360: 02 cb b4 12 23 c0 c1 56  b1 15 1b 43 c6 2b e3 36  ....#..V...C.+.6
00000370: c3 69 01 11 0a 77 c2 a2  f0 ed 8a a2 4a 04 35 95  .i...w......J.5.
00000380: 3a 73 f9 a9 8d 74 f9 1c  46 fc 02 3a 09 b4 70 1a  :s...t..F..:..p.
00000390: 7a 4b 82 b5 46 f3 66 39  c6 87 9d 14 12 f6 2a a0  zK..F.f9......*.
000003a0: 1c fa 7a ae 33 9f d2 ef  06 04 2a d1 3c 2e a6 a8  ..z.3.....*.<...
000003b0: 6d f7 74 dd d2 e0 a0 b6  4d 14 4a 61 e4 a8 de cd  m.t.....M.Ja....
000003c0: 3b db 70 cc e0 0a 7a 9e  70 a1 cd 23 fe f5 6b 1f  ;.p...z.p..#..k.
000003d0: 26 48 74 e7 ba c9 f3 ba  81 e5 14 e2 00 4f 95 ef  &Ht..........O..
000003e0: 43 00 68 20 d8 64 3d 03  29 69 d4 e6 0f b9 b7 c2  C.h .d=.)i......
000003f0: 9f 9a 1f 4c ce 83 ba 00  8a 3b 3a 06 23 22 4a 21  ...L.....;:.#"J!
00000400: 1f 9b 38 38 92 bd 2b 7e  5e 28 e9 6b ee 37 6c 67  ..88..+~^(.k.7lg
00000410: 0f 78 d2 d4 15 de 85 b2  ba 40 80 bb a3 6f 17 7b  .x.......@...o.{
00000420: 65 b4 7d 25 6e 3b a5 6c  9e cd 4c a1 ca 77 e7 3a  e.}%n;.l..L..w.:
00000430: 19 31 37 16 8f 29 1f e9  bd 1f a9 66 85 23 8e f7  .17..).....f.#..
00000440: d6 71 3a 6d 0a c8 18 d4  d4 35 9e fc cb e4 c3 ee  .q:m.....5......
00000450: 8b 4b 53 49 35 25 8c 9b  c4 74 11 9d 82 10 17 03  .KSI5%...t......
00000460: 03 01 19 77 b0 2b c6 79  1b 7f f3 67 da e5 f9 2a  ...w.+.y..g...*
00000470: 28 2c c8 46 9e 3e 5b e2  c7 99 c0 02 ef e5 d3 0f  (,.F.>[.........
00000480: 83 b0 db e4 7b 6f 4c fe  43 ff bd b8 45 5e b9 b2  ....{oL.C...E^..
00000490: bf 2c c8 46 eb b5 8e 25  d2 85 7f 7f 6b 1a 74 8a  .,.F...%..k.t.
000004a0: 92 67 f5 31 63 c4 88 4b  11 62 17 05 a6 c1 a6 02  .g.1c..K.b......
000004b0: dc dc ef ae 77 54 61 15  eb 93 1a 4a c5 17 1a 40  ....wTa....J...@
000004c0: 96 81 90 1e 62 46 bb 33  26 1c c4 a2 45 06 97 14  ....bF.3&...E...
000004d0: a2 2d 93 8a 09 ab 51 8e  b3 68 7a 6e 4a 11 87 d7  .-....Q..hznJ...
000004e0: 6d 00 04 68 2b fc 5e 51  0e f5 9c 93 25 28 f4 c4  m..h+.^Q....%(..
000004f0: 56 9c 66 77 65 50 f8 98  1a ad 4d b2 86 3b 98 a9  V.fweP....M..;..
00000500: 69 b1 8b bb fc 0c bf 6d  3f ab 3f 03 15 e8 c9 fa  i......m?.?.....
00000510: 88 f6 d1 f2 44 c8 1d c5  1a 98 9c 01 7e 53 fb e8  ....D.......~S..
00000520: 45 e1 ab 11 e1 a3 d9 8e  d2 6f ac 44 ed b3 f6 ae  E........o.D....
00000530: bb 99 47 74 fe 57 1e 60  8c 1f bd 44 80 0f 53 cb  ..Gt.W.`...D..S.
00000540: b0 0c f7 c5 7c 06 e5 26  a4 18 25 09 ee 4b 46 a5  ....|..&..%..KF.
00000550: 62 34 d5 31 94 70 ea 57  c9 26 a7 80 60 33 26 b8  b4.1.p.W.&..`3&.
00000560: b1 2a 0b c1 7f ac 16 78  27 e7 ec f8 2c 62 f9 25  .*....x'...,b.%
00000570: 60 1b 70 97 d2 cf 92 07  70 86 cc 10 17 03 03 00  `.p.....p.......
00000580: 45 15 ee 5c 89 20 5d 76  29 c0 4f 6c a5 b8 4a 46  E..\. ]v).Ol..JF
00000590: 7a 5f bb 33 df 5b 04 54  3a b9 bf ab b5 5b 6a d2  z_.3.[.T:....[j.
000005a0: 81 dd aa 31 26 77 81 8a  a0 a1 73 e6 57 99 a9 10  ...1&w....s.W...
000005b0: ce cd 54 56 7e b0 ce d7  6e 64 15 b6 bc 8d e2 e2  ..TV~...nd......
000005c0: 99 46 0b 64 4b b3                                 .F.dK.
Client didn't finish handshake
```

```
$ python3 inspectable/inspectable_client.py
========== send:  ssl ==========
00000000: 16 03 01 02 00 01 00 01  fc 03 03 47 ef a7 c5 e0  ...........G....
00000010: e9 27 50 3d 6f 3f a6 a6  1f 87 36 48 80 71 a6 ad  .'P=o?....6H.q..
00000020: 84 a3 62 c2 72 b4 00 b9  40 44 8e 20 47 6c ab 1f  ..b.r...@D. Gl..
00000030: 48 46 63 f4 8a ee 52 ea  e0 0b 2d 1d 19 fc 31 fb  HFc...R...-...1.
00000040: 02 e1 47 ed 94 bf 8c c6  c5 71 3b bd 00 3e 13 02  ..G......q;..>..
00000050: 13 03 13 01 c0 2c c0 30  00 9f cc a9 cc a8 cc aa  .....,.0........
00000060: c0 2b c0 2f 00 9e c0 24  c0 28 00 6b c0 23 c0 27  .+./...$.(.k.#.'
00000070: 00 67 c0 0a c0 14 00 39  c0 09 c0 13 00 33 00 9d  .g.....9.....3..
00000080: 00 9c 00 3d 00 3c 00 35  00 2f 00 ff 01 00 01 75  ...=.<.5./.....u
00000090: 00 00 00 17 00 15 00 00  12 6c 65 69 6c 61 6c 65  .........leilale
000000a0: 69 6c 6f 65 73 2e 6f 6e  69 6f 6e 00 0b 00 04 03  iloes.onion.....
000000b0: 00 01 02 00 0a 00 0c 00  0a 00 1d 00 17 00 1e 00  ................
000000c0: 19 00 18 00 23 00 00 00  16 00 00 00 17 00 00 00  ....#...........
000000d0: 0d 00 30 00 2e 04 03 05  03 06 03 08 07 08 08 08  ..0.............
000000e0: 09 08 0a 08 0b 08 04 08  05 08 06 04 01 05 01 06  ................
000000f0: 01 03 03 02 03 03 01 02  01 03 02 02 02 04 02 05  ................
00000100: 02 06 02 00 2b 00 09 08  03 04 03 03 03 02 03 01  ....+...........
00000110: 00 2d 00 02 01 01 00 33  00 26 00 24 00 1d 00 20  .-.....3.&.$...
00000120: 0a a9 ed f8 90 07 24 51  eb f3 81 7b 18 93 ab 6b  ......$Q...{...k
00000130: 59 15 4f 69 d0 10 29 66  25 ab 85 55 68 31 da 0b  Y.Oi..)f%..Uh1..
00000140: 00 15 00 c1 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000150: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000160: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000170: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000180: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000190: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001a0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001b0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001c0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001d0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001e0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001f0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000200: 00 00 00 00 00                                    .....
ssl.SSLCertVerificationError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: self signed certificate (_ssl.c:1129)
```

We can see that the client sends the initial handshake, the server responds and the client immediately crashes because the certificate that it has doesn't match. That means that the client can't connect to an unknown server. The TLS library says that the error is connected to the fact that the certificate is self-signed, since usually certificates used in TLS connections aren't self-signed.

Finally, we can test that the message integrity is preserved. Let's say that the medium that the client and the server are communicating isn't secure and can tampered with by some malicious actor. We will simulate that by editing a byte of the encrypted bid message, and checking whether the server accepts that as a valid message.

```
$ python3 inspectable/inspectable_server.py
What is the name of the item to be auctioned? Italian Greyhound NFT
Press enter to end the auction.
========== send:  ssl ==========
00000000: 16 03 03 00 7a 02 00 00  76 03 03 88 6f 25 58 a3  ....z...v...o%X.
00000010: 7f 9f 29 c5 74 0e 54 84  88 db 6b da b7 70 a0 7c  .).t.T...k..p.|
00000020: 34 1b 86 68 dc 85 66 bc  66 bd ec 20 4e 90 b9 e5  4..h..f.f.. N...
00000030: 78 5b 87 57 d9 cc 21 9a  d8 98 06 82 c8 f8 f5 9f  x[.W..!.........
00000040: 8f fa 11 7a 77 01 94 98  bb 49 fd b3 13 02 00 00  ...zw....I......
00000050: 2e 00 2b 00 02 03 04 00  33 00 24 00 1d 00 20 05  ..+.....3.$... .
00000060: c5 12 b9 37 bf 54 44 a8  7b 7e 19 6d dc 6d 83 e9  ...7.TD.{~.m.m..
00000070: 24 32 22 16 ba 9a 93 ad  db b5 ca 96 9f fa 25 14  $2"...........%.
00000080: 03 03 00 01 01 17 03 03  00 17 22 6f 28 ca 42 0b  .........."o(.B.
00000090: 42 af 59 0b cf 06 03 9f  3f 2c 78 c9 b5 9b e3 b8  B.Y.....?,x.....
000000a0: a5 17 03 03 03 b8 4d 57  f6 7d f3 63 b4 d6 ec 73  ......MW.}.c...s
000000b0: 71 05 76 42 c3 6b 45 e8  81 6f 76 e1 03 7f a4 61  q.vB.kE..ov...a
000000c0: cf 50 71 a7 27 4c 1d cf  fa 1b 6e 8c f7 ee 1e 37  .Pq.'L....n....7
000000d0: 47 bd ea 8a 49 0e d7 73  dd 12 eb d6 da 9a ce a2  G...I..s........
000000e0: f4 2a df f5 ee 9d 27 d7  8d 00 30 96 b3 4d 06 98  .*....'...0..M..
000000f0: 49 d4 79 a3 45 ef 38 58  cd 21 9a 12 eb 94 fe 7b  I.y.E.8X.!.....{
00000100: 9a ac a4 3b 16 5f c4 22  cf 8c 24 aa 86 b0 fe 1f  ...;._."..$.....
00000110: a6 be 04 e9 bb 03 da bf  20 ec 1e 48 83 1a a1 15  ........ ..H....
00000120: 87 a7 47 cc 1c 3c 37 7f  8d 77 40 9d 8c 09 c1 b9  ..G..<7.w@.....
00000130: 68 f1 d1 81 00 75 1e 38  12 cf 22 30 1f 30 c8 50  h....u.8.."0.0.P
00000140: 06 ab 68 e2 aa d3 90 57  af 8c 85 d6 f2 4c 08 7c  ..h....W.....L.|
00000150: c1 96 42 8f ea 3a 4b 0a  5c 70 da 07 d7 8f 94 49  ..B..:K.\p.....I
00000160: 05 8c b3 34 ff f1 fa 6b  cb e8 c6 32 75 ed 0c ec  ...4...k...2u...
00000170: 55 54 49 05 b8 da b7 dd  d1 a5 55 6c 45 95 bb 94  UTI.......UlE...
00000180: af 85 f6 db b1 51 df 45  29 01 f1 fb 1f 11 f2 21  .....Q.E)......!
00000190: c5 c3 8a c8 63 0b b9 22  76 00 f2 c4 d9 3b aa bf  ....c.."v....;..
000001a0: 7f 35 71 ba b9 30 79 fb  d8 e9 f1 a7 20 95 7a 20  5q..0y..... .z
000001b0: ba d8 09 1c 96 6c ad cb  3c 10 5f 4d 95 b1 78 e5  .....l..<._M..x.
000001c0: 9c 23 fc 84 72 4a 99 f3  2a a0 90 55 66 1d 90 80  .#..rJ..*..Uf...
000001d0: a7 1a 7c 33 c9 45 6b 75  36 bf 82 d2 17 cf 96 57  ..|3.Eku6......W
000001e0: 75 7a 32 cf 1c 8c 3c b6  c4 b3 18 20 c1 70 40 7a  uz2...<.... .p@z
000001f0: 41 47 3e 86 fb 5e f4 2c  87 c4 82 e6 c5 98 ea c8  AG>..^.,........
00000200: 7b 29 4c 2f db bc f3 7e  1c a4 f7 05 58 7f af d1  {)L/...~....X..
00000210: 96 31 66 14 b7 14 a4 22  06 13 5e 4a bf d2 ad 50  .1f...."..^J...P
00000220: e4 2f 26 1b 26 75 51 c8  c1 d9 f3 44 c4 16 71 88  ./&.&uQ....D..q.
00000230: 99 c0 f7 54 48 32 14 4a  85 69 0c 83 7c ca a2 0a  ...TH2.J.i..|...
00000240: d0 58 5d e8 87 5e 89 be  53 30 ee f5 3e 9e ea 92  .X]..^..S0..>...
00000250: 07 24 41 e8 8f 29 33 ef  b5 e6 27 aa 82 fd 5d 67  .$A..)3...'...]g
00000260: 08 41 7a a5 92 78 d9 af  21 c1 1a 67 5d b5 57 03  .Az..x..!..g].W.
00000270: ac 9c a3 cc b2 76 7c a4  c4 e6 9e 3d 35 18 fa 3a  .....v|....=5..:
00000280: 42 fc d1 75 a5 d4 f0 6c  74 c9 4e 71 64 a5 98 a3  B..u...lt.Nqd...
00000290: 64 ca a8 6a 7d 48 a4 54  80 c2 c8 d7 96 a0 8f 12  d..j}H.T........
000002a0: 14 bf d0 76 93 fe 17 b3  8a 76 cf 15 bf d2 e6 a0  ...v.....v......
000002b0: d7 8e 8d 36 7e 34 ef 87  d3 dd 95 d7 84 14 d2 0d  ...6~4..........
000002c0: 88 42 f8 e6 41 4c c3 d4  04 89 a2 e5 01 56 c3 78  .B..AL.......V.x
000002d0: e0 71 6a a7 1b 41 80 24  8c 2f ad a3 41 cd b1 93  .qj..A.$./..A...
000002e0: e0 8d 38 9a f1 93 6c 69  2e ae 22 ba 26 56 ae e4  ..8...li..".&V..
000002f0: ef b9 f6 83 fb 31 56 c6  dd 93 70 9d e9 12 5f 85  .....1V...p..._.
00000300: fd 7a 8b 97 51 fb 5a ba  00 94 be 99 ee 40 2d 52  .z..Q.Z......@-R
00000310: a5 c3 24 87 e6 a3 85 a5  93 9d 3e 61 dc 2f f4 bc  ..$.......>a./..
00000320: 28 c2 f4 d7 3c 16 cb 2f  a8 fe 6b eb 08 a4 26 14  (...<../..k...&.
00000330: 63 5c 42 8e 09 07 84 87  17 6e 7a 2c 4a c4 aa 78  c\B......nz,J..x
00000340: 76 72 ca d3 48 b4 9f 29  d3 d2 26 76 fe 08 7a 54  vr..H..)..&v..zT
00000350: 05 3e 50 78 04 9d fe a6  cd ca 32 33 df ae bb 3f  .>Px......23...?
00000360: d5 d6 e8 45 0d bd fc 90  87 55 fc 1b 43 6b 8f f5  ...E.....U..Ck..
00000370: 03 e6 8d 1c 61 c7 c1 3b  57 1d 25 9d 5c 4c 1d 5d  ....a..;W.%.\L.]
00000380: 4b e2 f6 79 2b 83 17 d5  1b 81 fc c5 30 d3 6e 6f  K..y+.......0.no
00000390: 9c df 4c 9a 68 31 57 2a  72 64 cd 5f 56 2c 23 b7  ..L.h1W*rd._V,#.
000003a0: 20 70 09 63 d9 0f ac c5  a9 ba 0d 92 54 d0 59 a4   p.c........T.Y.
000003b0: 81 e7 60 1d 3d fa c6 61  25 93 92 01 10 dd ed 4e  ..`.=..a%......N
000003c0: f6 b0 b3 61 63 5c dc ed  e3 58 f3 31 57 72 75 ef  ...ac\...X.1Wru.
000003d0: e1 2d 41 4e 3f dd 2a 96  f2 88 3d 11 d4 e9 f4 d5  .-AN?.*...=.....
000003e0: 91 d9 81 2d 1f be 67 ba  35 fe 2a bc e8 9e 41 48  ...-..g.5.*...AH
000003f0: e3 4a 1b 7e 1b 62 f5 6e  37 07 de e7 24 5b 82 76  .J.~.b.n7...$[.v
00000400: 67 68 e0 36 b2 2e 0d b0  ba c8 7f 14 4b 2a d2 71  gh.6.......K*.q
00000410: 36 8c 13 b0 64 c7 88 63  ff 9d 02 71 b6 b0 81 22  6...d..c...q..."
00000420: b2 43 03 8e 1b a6 79 21  97 15 8d d3 f4 8f b7 f9  .C....y!........
00000430: 59 74 04 50 d3 34 a9 7c  14 94 07 64 4f 15 bc 26  Yt.P.4.|...dO..&
00000440: e7 12 aa 5c ec f7 e9 db  00 36 22 ee a6 5a fd 5b  ...\.....6"..Z.[
00000450: 98 54 e0 7f be 9c c4 e8  20 7b 43 97 69 0e 17 03  .T..... {C.i...
00000460: 03 01 19 5a f8 f5 3f 72  65 e3 d7 85 46 d6 6c 89  ...Z..?re...F.l.
00000470: a1 84 f8 e5 88 7d 22 b6  14 a7 9e ce a3 02 99 27  .....}"........'
00000480: f3 d1 bc 32 b5 ea 93 08  07 14 65 c3 46 2c 38 cd  ...2......e.F,8.
00000490: 1f c8 73 cc e8 03 89 63  16 a8 da 5b e0 2e fc 10  ..s....c...[....
000004a0: d7 d3 a7 ff 31 9b a6 40  d1 d2 da 49 32 b9 eb 26  ....1..@...I2..&
000004b0: 5f 37 8d c1 10 17 10 58  de b2 4e 60 34 cc 00 91  _7.....X..N`4...
000004c0: 29 c4 ec 5b 0d ae e0 49  83 b4 df 37 38 12 7a ec  )..[...I...78.z.
000004d0: 65 03 fb 5c cd 7a 6e 68  cc b0 fa 49 5b 22 fe ab  e..\.znh...I["..
000004e0: 0d 53 ea 0e 6c 95 1e 4f  4b 81 cf fe ad 4a 51 9f  .S..l..OK....JQ.
000004f0: e6 70 e2 b0 aa 64 20 a6  1e f6 a2 af 67 b0 a9 c6  .p...d .....g...
00000500: 2e 5a ef b0 55 97 36 03  2a 57 1b f2 53 71 b7 6b  .Z..U.6.*W..Sq.k
00000510: 32 13 7b d2 1d 04 0d 17  70 17 7f a0 77 82 21 0d  2.{.....p..w.!.
00000520: e4 3c 66 4a e6 8c f9 97  2a 7d 33 7c bf a9 b2 bc  .<fJ....*}3|....
00000530: e2 e3 c0 1b 9b 95 71 e8  f3 ae 7c f5 d9 5d 72 2c  ......q...|..]r,
00000540: 13 fc c2 c9 48 f8 a1 33  6d 3e da 56 e9 49 e7 98  ....H..3m>.V.I..
00000550: df 87 ac f0 ad 38 5d 29  96 f6 37 14 d2 f1 42 6b  .....8])..7...Bk
00000560: 30 01 f7 80 9e 21 eb e1  b6 6d 4d 49 3b 95 77 86  0....!...mMI;.w.
00000570: f2 79 d8 94 6c a9 04 cd  50 4e 91 59 17 03 03 00  .y..l...PN.Y....
00000580: 45 59 e8 c9 34 17 fe 26  ef ee 91 05 ed 66 82 87  EY..4..&.....f..
00000590: be 25 4e e9 b0 7b 6f fe  b7 73 d5 f1 d0 c1 9b 5c  .%N..{o..s.....\
000005a0: 21 6d 7d e1 ef 60 46 ce  be af cc ef a6 4c d3 00  !m}..`F......L..
000005b0: 72 50 75 8f ae 4d a4 46  39 d2 ed 27 a8 7a 4c 58  rPu..M.F9..'.zLX
000005c0: a7 ec 55 29 9b 62                                 ..U).b
========== send:  ssl ==========
00000000: 17 03 03 00 fa 21 c7 d7  ff 7f 2b 1d ea 8c 7b 28  .....!...+...{(
00000010: 56 20 2c 70 f1 c5 45 26  25 c4 10 7b c8 5c c9 a4  V ,p..E&%..{.\..
00000020: a3 2b f9 03 78 a0 c8 b8  ee 8a 82 86 f0 ed 26 92  .+..x.........&.
00000030: 85 f7 ff 6c 77 9a b8 79  2a bd 34 7e 49 1f 16 73  ...lw..y*.4~I..s
00000040: de d1 85 2b ea aa df 82  4c f0 4b e4 31 68 4b 95  ...+....L.K.1hK.
00000050: 5d 2c 49 6c 27 5c 82 15  c8 c0 fa 02 12 87 65 f1  ],Il'\........e.
00000060: 15 9d 66 7c 7a 49 18 5a  68 a5 73 f6 eb 33 f1 68  ..f|zI.Zh.s..3.h
00000070: 83 94 83 12 2f 3a 67 ed  9c 0d bd d7 0f 25 1c 7e  ..../:g......%.~
00000080: 43 19 dc 80 23 eb 4a 55  ae 74 19 d6 a4 44 da ed  C...#.JU.t...D..
00000090: 75 17 49 f2 0b e0 e8 e2  d1 7e 77 fb 46 2f b4 90  u.I......~w.F/..
000000a0: 0b c1 10 06 37 7f 40 9e  c9 5a 09 0b f6 2b dd 28  ....7@..Z...+.(
000000b0: 7e 39 cd 63 b4 cc ba 6a  a2 dd b5 1c d3 5f 78 1d  ~9.c...j....._x.
000000c0: 70 a5 e5 3c 9d 7e df eb  cb c4 08 17 05 3b 5f 29  p..<.~.......;_)
000000d0: 83 f7 ea ea 26 cd 65 ae  d1 82 28 bf 8c 3d d6 c6  ....&.e...(..=..
000000e0: 4f 1d e2 db 80 e4 d5 4a  de 39 82 51 e1 2b de 08  O......J.9.Q.+..
000000f0: 32 94 c4 08 9a eb 7d 21  b9 aa e8 d5 90 e7 75 17  2.....}!......u.
00000100: 03 03 00 fa b3 18 04 03  a4 ae 13 0c e8 51 55 ec  .............QU.
00000110: f9 ca 7f dd 12 d0 77 14  9f 2f 2c 34 ce 5a 7a 0e  .....w../,4.Zz.
00000120: f4 85 d9 d4 c9 08 12 4e  da 86 14 e6 f2 54 24 94  .......N.....T$.
00000130: bd c4 f0 63 51 70 d5 06  3b 4d 5a 90 25 ba ba fc  ...cQp..;MZ.%...
00000140: f5 95 8a 59 fb 79 97 07  61 0d 34 30 bc ff 94 60  ...Y.y..a.40...`
00000150: 63 6a b1 fb 6a 52 8d 76  65 83 ab 0e f7 50 7e 2d  cj..jR.ve....P~-
00000160: 07 1b 26 45 29 1e 7a b0  31 9c df 77 7d 99 18 79  ..&E).z.1..w}..y
00000170: 0b bc 3c 57 f8 b6 db ee  52 fa 3e e2 68 ca 65 3b  ..<W....R.>.h.e;
00000180: 79 7a 76 fe fe ca 48 70  67 d4 00 20 b6 48 2b 60  yzv...Hpg.. .H+`
00000190: e3 58 e4 94 44 b6 63 3e  14 81 c9 64 97 31 16 ca  .X..D.c>...d.1..
000001a0: fb 51 21 72 63 3b 38 bd  2f 2a 7c 8f fb 0a 9c d5  .Q!rc;8./*|.....
000001b0: 0d 6b 55 f5 62 25 5c ac  cd 2b 53 62 ec d8 92 ca  .kU.b%\..+Sb....
000001c0: ba ba 31 72 9f c9 e3 cf  a8 45 7e 16 65 a6 06 a2  ..1r.....E~.e...
000001d0: 42 f7 5f 10 ba 6b 4c c8  bf 50 24 c1 d7 4a 94 e7  B._..kL..P$..J..
000001e0: 79 f6 e8 27 ec d3 45 81  27 5b bb c2 01 ce c2 8f  y..'..E.'[......
000001f0: a2 69 e5 6b f2 d2 c4 89  f4 34 1e ee ee 7c        .i.k.....4...|
Cipher: ('TLS_AES_256_GCM_SHA384', 'TLSv1.3', 256)
======== send: original ========
00000000: 7b 22 74 79 70 65 22 3a  20 22 57 65 6c 63 6f 6d  {"type": "Welcom
00000010: 65 22 2c 20 22 64 61 74  61 22 3a 20 7b 22 69 74  e", "data": {"it
00000020: 65 6d 5f 6e 61 6d 65 22  3a 20 22 49 74 61 6c 69  em_name": "Itali
00000030: 61 6e 20 47 72 65 79 68  6f 75 6e 64 20 4e 46 54  an Greyhound NFT
00000040: 22 7d 7d 0a                                       "}}.
========== send:  ssl ==========
00000000: 17 03 03 00 55 13 40 56  87 da f1 f2 c4 81 6e de  ....U.@V......n.
00000010: bc c8 a3 66 cf 3a ad cf  29 9b ad 7d db 49 e2 76  ...f.:..)..}.I.v
00000020: 69 6d 9b 72 ec a3 a8 26  76 3c bc fb 48 63 82 a8  im.r...&v<..Hc..
00000030: 0d 40 84 af 8f ef 4d 15  4f 74 a7 eb 99 4a d7 98  .@....M.Ot...J..
00000040: f5 fe 9b 6e 9c 4c 9f f3  9c 79 9e 26 2d 2d 20 3c  ...n.L...y.&-- <
00000050: 7f 03 90 5b 64 0b ca 53  95 3f                    ..[d..S.?
ssl.SSLError: [SSL: DECRYPTION_FAILED_OR_BAD_RECORD_MAC] decryption failed or bad record mac (_ssl.c:2633)
```

```
$ python3 inspectable/inspectable_client.py --edit
========== send:  ssl ==========
00000000: 16 03 01 02 00 01 00 01  fc 03 03 60 ad 2e 73 fa  ...........`..s.
00000010: ff 8e 96 0a 41 9e cf 65  90 bb 55 1c 91 07 e4 43  ....A..e..U....C
00000020: 2e 2c e1 99 03 81 e7 68  f1 3d b6 20 4e 90 b9 e5  .,.....h.=. N...
00000030: 78 5b 87 57 d9 cc 21 9a  d8 98 06 82 c8 f8 f5 9f  x[.W..!.........
00000040: 8f fa 11 7a 77 01 94 98  bb 49 fd b3 00 3e 13 02  ...zw....I...>..
00000050: 13 03 13 01 c0 2c c0 30  00 9f cc a9 cc a8 cc aa  .....,.0........
00000060: c0 2b c0 2f 00 9e c0 24  c0 28 00 6b c0 23 c0 27  .+./...$.(.k.#.'
00000070: 00 67 c0 0a c0 14 00 39  c0 09 c0 13 00 33 00 9d  .g.....9.....3..
00000080: 00 9c 00 3d 00 3c 00 35  00 2f 00 ff 01 00 01 75  ...=.<.5./.....u
00000090: 00 00 00 17 00 15 00 00  12 6c 65 69 6c 61 6c 65  .........leilale
000000a0: 69 6c 6f 65 73 2e 6f 6e  69 6f 6e 00 0b 00 04 03  iloes.onion.....
000000b0: 00 01 02 00 0a 00 0c 00  0a 00 1d 00 17 00 1e 00  ................
000000c0: 19 00 18 00 23 00 00 00  16 00 00 00 17 00 00 00  ....#...........
000000d0: 0d 00 30 00 2e 04 03 05  03 06 03 08 07 08 08 08  ..0.............
000000e0: 09 08 0a 08 0b 08 04 08  05 08 06 04 01 05 01 06  ................
000000f0: 01 03 03 02 03 03 01 02  01 03 02 02 02 04 02 05  ................
00000100: 02 06 02 00 2b 00 09 08  03 04 03 03 03 02 03 01  ....+...........
00000110: 00 2d 00 02 01 01 00 33  00 26 00 24 00 1d 00 20  .-.....3.&.$...
00000120: f6 61 2a cf f5 a6 84 ff  7d 86 54 6a 47 ba ec a9  .a*.....}.TjG...
00000130: ad 2a 16 63 20 cb ab 2b  d1 3b 82 81 dc 04 49 5b  .*.c ..+.;....I[
00000140: 00 15 00 c1 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000150: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000160: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000170: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000180: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000190: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001a0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001b0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001c0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001d0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001e0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
000001f0: 00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
00000200: 00 00 00 00 00                                    .....
Do you want to edit this message? [y/N] n
========== send:  ssl ==========
00000000: 14 03 03 00 01 01 17 03  03 00 45 9b 3c e0 44 58  ..........E.<.DX
00000010: 75 1b a5 64 5c f2 1b 5d  53 3b 44 fd 17 99 7b 30  u..d\..]S;D...{0
00000020: e3 8d 2e 20 48 be 56 76  25 3d 78 05 c7 65 44 a5  ... H.Vv%=x..eD.
00000030: be 5a ab 8e 71 41 20 b5  d4 b9 f6 cc bb 8a 43 6f  .Z..qA .......Co
00000040: 4c 0b d8 18 a1 c8 23 3f  7f 30 fe f1 57 14 08 4e  L.....#?0..W..N
Do you want to edit this message? [y/N] n
Cipher: ('TLS_AES_256_GCM_SHA384', 'TLSv1.3', 256)
Auctioned item: Italian Greyhound NFT
Enter your bid: 10
======== send: original ========
00000000: 7b 22 74 79 70 65 22 3a  20 22 42 69 64 22 2c 20  {"type": "Bid",
00000010: 22 64 61 74 61 22 3a 20  7b 22 76 61 6c 75 65 22  "data": {"value"
00000020: 3a 20 31 30 7d 7d 0a                              : 10}}.
========== send:  ssl ==========
00000000: 17 03 03 00 38 df 6e 0c  12 69 3b 28 e6 56 1e 82  ....8.n..i;(.V..
00000010: 23 3e 8b 2e da 6c 8f e2  4b 69 b0 ab c1 ff cc 97  #>...l..Ki......
00000020: 34 7d 3c 19 cf 37 41 7e  6b 5a 35 97 b1 50 74 2c  4}<..7A~kZ5..Pt,
00000030: 6e 22 f2 4e 5b d4 2b 1e  03 60 a4 1c 36           n".N[.+..`..6
Do you want to edit this message? [y/N] y
Enter the index in hexadecimal: 10
Enter the byte in hexadecimal: 4c
========= send: edited =========
00000000: 17 03 03 00 38 df 6e 0c  12 69 3b 28 e6 56 1e 82  ....8.n..i;(.V..
00000010: 4c 3e 8b 2e da 6c 8f e2  4b 69 b0 ab c1 ff cc 97  L>...l..Ki......
00000020: 34 7d 3c 19 cf 37 41 7e  6b 5a 35 97 b1 50 74 2c  4}<..7A~kZ5..Pt,
00000030: 6e 22 f2 4e 5b d4 2b 1e  03 60 a4 1c 36           n".N[.+..`..6
```

We can see by the above messages that the server raises an exception because the encrypted message that it has received is malformed. This kind of error is unexpected to happen in most scenarios since TCP rarely fails, and the most likely culprit is something that is trying to tamper the messages.

With this, we can attest to the three security properties that TLS guarantees: Confidentiality (nobody can read your messages), Authenticity (you're talking to a known server) and Integrity (the messages can't be tampered with).

## Where can I find this code?

All the code used in this tutorial is available in [Fernando's leilalões repository](https://gitlab.com/fer22f/leilaloes).
