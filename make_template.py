import re


def replace_with_file(g):
    file_name = g.group(1)
    with open(file_name) as f:
        return f.read().strip()


with open("tutorial.template.md") as f:
    replaced = re.sub("{{(.+)}}", replace_with_file, f.read())

with open("tutorial.md", "w") as f:
    f.write(replaced)
